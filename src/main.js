import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store' 
import 'bootstrap/dist/css/bootstrap.rtl.min.css'; 
import "bootstrap/dist/css/bootstrap.min.css" 
import "bootstrap";
import '@/assets/css/style.css';  

import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome"; 
import { library } from "@fortawesome/fontawesome-svg-core";  
import "@fortawesome/free-solid-svg-icons";
import { fas } from '@fortawesome/free-solid-svg-icons'
import { fab } from '@fortawesome/free-brands-svg-icons'

library.add( fas , fab ); 
import { dom } from "@fortawesome/fontawesome-svg-core";
dom.watch(); 


createApp(App)
.component("font-awesome-icon", FontAwesomeIcon)
.use(store).use(router).mount('#app')