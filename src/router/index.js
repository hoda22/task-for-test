import { createRouter, createWebHistory } from 'vue-router' 

import Home from '../views/home/Index.vue'
import UserList from '../views/users/List.vue'
import UserShow from '../views/users/Show.vue' 

import ProductsList from '../views/products/List.vue'
import ProductsShow from '../views/products/Show.vue'

import PageNotFoundView from '../views/PageNotFoundView.vue'

const routes = [

  {
    path: '/',
    name: 'Home',
    component: Home ,
    meta: {
      text: 'الرئيسيه' ,
      isParent:true
    } ,
  },

  {
    path: '/users',
    name: 'UserList',
    component: UserList ,
    meta: {
      text: 'الصفحه الرئيسيه -  المستخدمين' ,
      isParent:true
    } ,
    children: [
      {
        // child route record
        path: 'users',
        name: 'users',
        meta: {
          text: 'الصفحه الرئيسيه -  المستخدمين'
        }
      }, 
    ] 
  },

  {
    // child route record
    path: '/user/show',
    component: UserShow ,
    name: 'users-show',
    meta: {
      text: 'عرض مستخدم'
    }
  },
  {
    path: '/',
    redirect: '/products' ,
    name: 'ProductsList', 
    meta: {
      text: 'الصفحه الرئيسيه -  المنتجات'
    } ,
    children: [
      {
        // child route record
        path: 'products',
        name: 'products',
        component: ProductsList ,
        meta: {
          text: 'الصفحه الرئيسيه -  المنتجات' ,
          isParent:true
        }
      },
      
    ]


  },
  {
    // child route record
    path: '/product/show',
    name: 'productShow',
    component: ProductsShow ,
    meta: {
      text: 'عرض المنتج'
    }
  },
  { 
    path: "/:pathMath(.*)*", 
    name: 'PageNotFoundView',
    component: PageNotFoundView 
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes ,
  linkActiveClass: 'active-link',
  linkExactActiveClass: 'exact-active-link',
})

export default router
