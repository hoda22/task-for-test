import { createStore } from 'vuex' 
export default createStore({
   state:{
         products: [
            {
            id: 1,
            name:"banana",  
            created_at:"123-223"
            } ,
            {
            id: 2,
            name:"orange",
            created_at:"123-223"
            } ,
         {
            id: 3,
            name:"tangerine",
            created_at:"123-223"
         } ,
         {
            id: 4,
            name:"Pineapple",
            created_at:"123-223"
         } ,
         {
            id: 5,
            name:"cantaloupe",
            created_at:"123-223"
         } ,
         {
            id: 6,
            name:"Mamdouh",
            created_at:"123-223"
         } ,     
         {
            id: 7,
            name:"dates",
            created_at:"123-223",
   
         } ,     
         {
            id: 8,
            name:"mango", 
            created_at:"123-223"
         }  
         ] ,
   },
   getters: {
      getProducts( state ){
         return state.products
      } ,
      getProductById: (state) => (id) => {
         return state.products.find(product => product.id === id)
      }
   },

   mutations:{
      UPDATE_PRODUCTS(state, products) {
         state.products = products
      } ,

      UPDATE_PRODUCT(state, products) {
         state.products = products
      } ,

      REMOVE_PRODUCT(state, products) {
         state.products = products
      } ,
   } ,

   actions:{ 
      getProductAction({commit , state}, product){ 
         let id = 1 
         const convertProxyToArray = Object.assign([], state.products); 
         if( convertProxyToArray.length ){
            id = Number( convertProxyToArray[ convertProxyToArray.length - 1].id) + 1
            product.id = id;  
         }
         commit("UPDATE_PRODUCTS" , state.products.concat(product))  
      } ,

      UpdateProductAction({commit , state}, values){ 
         state.products[values.id - 1] = values
         commit("UPDATE_PRODUCTS" , state.products )  
      } ,

      RemoveProductAction({ commit , state } , item){ 
         state.products = state.products.filter((product) => {
            return product.id != item.id;
         });  
         commit("REMOVE_PRODUCT" , state.products)  
      }
   } ,
  modules: {
  }
})
