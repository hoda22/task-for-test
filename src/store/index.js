import { createStore } from 'vuex' 
export default createStore({
   state:{
      users: [
         {
           id: 1,
           name:"Mahmoud",
           email:"Mahmoud@email.com",
           jop:"Full stack",
           created_at:"123-223"
         } ,
         {
           id: 2,
           name:"Ahmed",
           email:"Ahmed@email.com",
           jop:"Full stack",
           created_at:"123-223"
         } ,
        {
           id: 3,
           name:"Sameh",
           email:"Mahmoud@email.com",
           jop:"Full stack",
           created_at:"123-223"
        } ,
        {
           id: 4,
           name:"Fatma Sakr",
           email:"Ahmed@email.com",
           jop:"Full stack",
           created_at:"123-223"
        } ,
        {
           id: 5,
           name:"Amr",
           email:"Mahmoud@email.com",
           jop:"Full stack",
           created_at:"123-223"
        } ,
        {
           id: 6,
           name:"Mamdouh",
           email:"Ahmed@email.com",
           jop:"Full stack",
           created_at:"123-223"
        } ,     
        {
           id: 7,
           name:"Ragaey",
           email:"Ahmed@email.com",
           jop:"Full stack",
           created_at:"123-223",
 
        } ,     
        {
           id: 8,
           name:"Mena",
           email:"Ahmed@email.com",
           jop:"Full stack",
           created_at:"123-223"
        } , 
        {
           id: 9,
           name:"Hota",
           email:"Ahmed@email.com",
           jop:"Full stack",
           created_at:"123-223"
        }   
      ] ,

      products: [
         {
           id: 1,
           name:"banana",  
           created_at:"123-223"
         } ,
         {
           id: 2,
           name:"orange",
           created_at:"123-223"
         } ,
        {
           id: 3,
           name:"tangerine",
           created_at:"123-223"
        } ,
        {
           id: 4,
           name:"Pineapple",
           created_at:"123-223"
        } ,
        {
           id: 5,
           name:"cantaloupe",
           created_at:"123-223"
        } ,
        {
           id: 6,
           name:"Mamdouh",
           created_at:"123-223"
        } ,     
        {
           id: 7,
           name:"dates",
           created_at:"123-223",
 
        } ,     
        {
           id: 8,
           name:"mango", 
           created_at:"123-223"
        }  
      ] ,

      breadcrumbs:[
         {
            path:"/",
            name:"الصفحه الرئيسيه"
         },
      ]
   },
   getters: {
      getUsers( state ){
         return state.users
      } ,
      getUserById: (state) => (id) => {
         return state.users.find(user => user.id === id)
      } , 
      
      getProducts( state ){
         return state.products
      } ,
      getProductById: (state) => (id) => {
         return state.products.find(product => product.id === id)
      },

      getBreadcrumb(state){
         return state.breadcrumbs
      } ,

       
   },

   mutations:{
      UPDATE_USERS(state, users) {
         state.users = users
      } ,

      UPDATE_USER(state, users) {
         state.users = users
      } ,

      REMOVE_USER(state, users) {
         state.users = users
      } ,


      UPDATE_PRODUCTS(state, products) {
         state.products = products
      } ,

      UPDATE_PRODUCT(state, products) {
         state.products = products
      } ,


      UPDATE_BREADCRUMBS(state, breadcrumbs) {
         state.breadcrumbs = breadcrumbs
      } ,

      REMOVE_PRODUCT(state, products) {
         state.products = products
      } ,
   } ,

   actions:{ 
      getUsers({ commit }) {
         console.log(commit)
      },   
      AddUserAction({commit , state}, user){ 
         let id = 1 
         const convertProxyToArray = Object.assign([], state.users); 
         if( convertProxyToArray.length ){
            id = Number( convertProxyToArray[ convertProxyToArray.length - 1].id) + 1
            user.id = id;  
         }
         commit("UPDATE_USERS" , state.users.concat(user))  
      } ,

      UpdateUserAction({commit , state}, values){ 
         values.jop = "Full stack"
         values.created_at ="51165/889"
         state.users[values.id - 1] = values
         commit("UPDATE_USERS" , state.users )  
      } ,

      RemoveUserAction({ commit , state } , item){ 
         state.users = state.users.filter((user) => {
            return user.id != item.id;
         });  
         commit("REMOVE_USER" , state.users)  
      },

      AddProductAction({commit , state}, product){ 
         let id = 1 
         const convertProxyToArray = Object.assign([], state.products); 
         if( convertProxyToArray.length ){
            id = Number( convertProxyToArray[ convertProxyToArray.length - 1].id) + 1
            product.id = id;  
         }
         commit("UPDATE_PRODUCTS" , state.products.concat(product))  
      } ,

      UpdateProductAction({commit , state}, values){ 
         state.products[values.id - 1] = values
         commit("UPDATE_PRODUCTS" , state.products )  
      } ,

      RemoveProductAction({ commit , state } , item){ 
         state.products = state.products.filter((product) => {
            return product.id != item.id;
         });  
         commit("REMOVE_PRODUCT" , state.products)  
      } ,


      AddItemToBreadcrumbAction({ commit , state } , item){
         commit("UPDATE_BREADCRUMBS" , state.breadcrumbs.concat(item))  
      } ,

      updateBreadcrumbAction({ commit } , items){
         commit("UPDATE_BREADCRUMBS" , items )  
      } ,


      ressetBreadcrumbAction({ commit }, item){
         let items = [{
            path:"/",
            name:"الصفحه الرئيسيه"
         }]
         items.push(item)
         commit("UPDATE_BREADCRUMBS" , items )  
      }
   } ,
  modules: {
  }
})
